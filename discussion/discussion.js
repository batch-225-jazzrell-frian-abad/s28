// MongoDB Operations
// For creating or inserting data into database

db.users.insert({
    firstName: "Ely",
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "534776",
        email: "ely@eraserheads.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
})

// for insert name documents


db.users.insertMany([
    {
        firstName: "Chito",
        lastName: "Miranda",
        age: 43,
        contact: {
            phone: "5347786",
            email: "chito@parokya.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Francis",
        lastName: "Magalona",
        age: 61,
        contact: {
            phone: "5347786",
            email: "francisM@email.com"
        },
        courses: ["React", "Laravel", "SASS"],
        department: "none"
    }
])

// Insert One document

db.users.insert(
    {
        firstName: "Gloc-9",
        lastName: "Walang Apelyido",
        age: 43,
        contact: {
            phone: "5336234",
            email: "gloc@parokya.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    }

);


// ==========================================

// for querying all the data in database
// For finding documents


// FIND ALL
db.users.find();

// FIND [One]
db.users.find({firstName: "Francis", age: 61});
    // or 
db.users.find({firstName: "Francis"});

// ============================================

// DELETE [ONE]

db.users.deleteOne({
    firstName: "Ely"
});

// DELETE Many

db.users.deleteMany({
    department: "DOH"
});

// ============================================

// UPDATE one document

db.users.updateOne(
    {
        firstName: "Chito"
    },
    {
        $set: {
            lastName: "Esguerra"
        }
    }
);

// UPDATE to many document

db.users.updateMany(
    {
        department: "none"
    },
    {
        $set: {
            department: "HR"
        }
    }
);